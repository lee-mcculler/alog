#!/bin/bash

ID=`id -u`

COOKIE_FILE="/tmp/ecpcookie.u$ID"
OUT_FILE="/tmp/alog_out_$ID_$$"

function usage() {
    echo "Usage:"
    echo "To post an entry      $0 --post|-p [-d] section task title text_filename https://host/<logbook>/ws/ <attachments>"
    echo "Version information   $0 --version|-v"
    echo "This help             $0 --help|-h"
    echo "Version: 0.03"
    echo ""
    echo "-d dump debug information to the terminal"
    echo ""
    echo "You must have done an ecp-cookie-init to the logbook first"
    exit 1
}

# some utility functionality for deleting temporary files
declare -a on_exit_items

function on_exit()
{
    for i in "${on_exit_items[@]}"
    do
        eval $i
    done
}

function add_on_exit()
{
    local n=${#on_exit_items[*]}
    on_exit_items[$n]="$*"
    if [[ $n -eq 0 ]]; then
        trap on_exit EXIT
    fi
}

DEBUG=

# check arguments now
if [ $# -lt 2 ]; then
	usage;
fi

if [ $1 != "--post" -a $1 != "-p" ]; then
    usage;
fi

shift 1

if [ "$1" == "-d" ]; then
    DEBUG=1
    shift 1
    echo "Debugging enabled"
fi

if [ $# -lt 5 ]; then
    usage;
fi

attachment_count=0

section=$1
task=$2
title=$3
text=$4
target=$5
attachments=


# verify that the target is of the form https://
if [[ ! "$target" =~ ^https:// ]]
then
    echo "Target is not of the form https://..."
    exit 1
fi

if [ ! -f $text -o ! -r $text ]; then
	echo "could not find the file containing the report text"
	exit 1
fi
text="$text"

shift 5
while (( $# )); do
	if [ ! -r $1 ]; then
		echo "could not find attachment file $1"
		exit 1
	fi
	if [ ! -f $1 ]; then
		echo "attachment $1 is not a regular file"
		exit 1
	fi
	attachments="$attachments --form WS_ATTACHMENT_$attachment_count=@$1"
	((attachment_count=$attachment_count+1))
	shift
done

add_on_exit rm -f $OUT_FILE

curl --silent -c $COOKIE_FILE -b $COOKIE_FILE -s \
	--form WS_VER=2.0 --form "WS_TITLE=$title" \
	--form WS_TEXT=\<$text \
	--form "WS_SECTION=$section" --form "WS_TASK=$task" \
	--form WS_TEXT_FORMAT=1 \
	--form WS_NUM_ATTACHMENTS=$attachment_count $attachments \
	$target > $OUT_FILE

cat $OUT_FILE

grep -q "report_id" $OUT_FILE
if [ $? -eq 0 ]; then
    return_val=0
else
    return_val=-1
fi

exit $return_val
