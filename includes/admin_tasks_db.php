<?php
require_once("initVar.php");

if (!oslUser::checkIfAdmin($userID)) {
	die ("Restricted access");
}


function doUpdateLastComment() {
	$rows = oslDAO::executePrepared("SELECT reportID, parentFK, lastCommentDate
								 FROM tblReports
								 WHERE parentFK <> 0", "", array());
	if (count($rows)==0) {
		echo "Unable to list the report and comment dates\n";
		return;
	}
	foreach ($rows as $row) {
		oslDAO::setLastCommentDateOnParents($row['parentFK'], $row['lastCommentDate']);
	}
}

function doUpdateTags() {
	// $callRep, $userID, $reportAuthors, $reportText, $tagList
	$rows = oslDAO::executPrepared("SELECT reportID, authorFK, authorNames, reportText
								 FROM tblReports", "", array());
	if (count($rows)==0) {
		echo "Unable to list the reports\n";
		return;
	}
	foreach ($rows as $row) {
		// for each report
		$tagRows = oslDAO::executePrepared("SELECT tagFK
										FROM tblReportTags, tblTags
										WHERE reportFK = ?
											AND tagID = tagFK
											AND tblTags.type = 0", "i", $row['reportID']);
		if (count($tagRows)==0) {
			echo "Unable to enumerate tags\n";
			return;
		}
		$tags = array();
		foreach ($tagRows as $tagRow) {
			$tags[] = $tagRow['tagFK'];
		}
		// now update the report
		$tags = oslDAO::getAnnotatedTagList($row["reportID"], $row["authorFK"], $row["authorNames"], $row["reportText"], $tags);
		oslDAO::setReportTags($row["authorFK"], $row["reportID"], $tags);
	}
}










if ($_SERVER['REQUEST_METHOD'] != "POST") {
?><!DOCTYPE html>
<head>
</head>
<body>
	<h2>Admin jobs</h2>
	<ul>
		<li>
			<form method="POST" action="">Update all the lastCommentDate fields<input type="hidden" name="action" value="update_last_comment" /><input type="submit" value="Update lastComment" /></form>
		</li>
		<li>
			<form method="POST" action="">Update all the system tags/annotations fields<input type="hidden" name="action" value="update_tags" /><input type="submit" value="Update system tags" /></form>
		</li>
	</ul>
</body>
</html>
<?php
} else {
	if ($_POST["action"] === "update_last_comment") {
		doUpdateLastComment();
		echo "\n\nDone\n";
	} else if ($_POST["action"] === "update_tags") {
		doUpdateTags();
		echo "\n\nDone\n";
	}
}
?>
