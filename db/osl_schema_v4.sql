-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: osl
-- ------------------------------------------------------
-- Server version	5.5.35-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblAppInfo`
--

DROP TABLE IF EXISTS `tblAppInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblAppInfo` (
  `appID` int(11) NOT NULL AUTO_INCREMENT,
  `appName` text NOT NULL,
  `appOpen` int(11) NOT NULL DEFAULT '0',
  `appRestrict` int(11) NOT NULL DEFAULT '0',
  `appLoginMethod` int(11) NOT NULL DEFAULT '0',
  `appMailDomain` text NOT NULL,
  `appMaxFileSize` double NOT NULL DEFAULT '10000000',
  `appUseThumbnails` int(11) NOT NULL DEFAULT '0',
  `ldapHost` text NOT NULL,
  `ldapDN` text NOT NULL,
  PRIMARY KEY (`appID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

-- 
-- Dumping data for table `tblAppInfo`
-- 

INSERT INTO `tblAppInfo` VALUES (1, 'OS', 0, 0, 0, '', 10000000, 1, '', '');

--
-- Table structure for table `tblContents`
--

DROP TABLE IF EXISTS `tblContents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblContents` (
  `contentID` int(11) NOT NULL AUTO_INCREMENT,
  `contentName` text NOT NULL,
  `orderPos` int(11) NOT NULL DEFAULT '0',
  `accessLevel` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contentID`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 COMMENT='Section information.';
/*!40101 SET character_set_client = @saved_cs_client */;

-- 
-- Dumping data for table `tblContents`
-- 

INSERT INTO `tblContents` VALUES (1, 'Home', 1, 0);
INSERT INTO `tblContents` VALUES (2, 'Search', 2, 0);
INSERT INTO `tblContents` VALUES (3, 'Add report', 3, 2);
INSERT INTO `tblContents` VALUES (4, 'Help', 6, 0);
INSERT INTO `tblContents` VALUES (5, 'L-mail', 5, 2);
INSERT INTO `tblContents` VALUES (6, 'Drafts', 4, 2);
INSERT INTO `tblContents` VALUES (7, 'Admin', 7, 1);
INSERT INTO `tblContents` VALUES (8, 'Users', 8, 1);

--
-- Table structure for table `tblFileTypes`
--

DROP TABLE IF EXISTS `tblFileTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblFileTypes` (
  `fileTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `fileType` text NOT NULL,
  `fileMimeType` text NOT NULL,
  `fileImage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fileTypeID`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

-- 
-- Dumping data for table `tblFileTypes`
-- 

INSERT INTO `tblFileTypes` VALUES (1, 'doc', 'application/msword', 0);
INSERT INTO `tblFileTypes` VALUES (2, 'pdf', 'application/pdf', 0);
INSERT INTO `tblFileTypes` VALUES (3, 'xls', 'application/vnd.ms-excel', 0);
INSERT INTO `tblFileTypes` VALUES (4, 'ppt', 'application/vnd.ms-powerpoint', 0);
INSERT INTO `tblFileTypes` VALUES (8, 'jpg', 'image/jpeg', 1);
INSERT INTO `tblFileTypes` VALUES (9, 'png', 'image/png', 1);
INSERT INTO `tblFileTypes` VALUES (10, 'gif', 'image/gif', 1);
INSERT INTO `tblFileTypes` VALUES (11, 'tif', 'image/tiff', 0);
INSERT INTO `tblFileTypes` VALUES (12, 'ps', 'application/postscript', 0);
INSERT INTO `tblFileTypes` VALUES (13, 'txt', 'text/plain', 0);
INSERT INTO `tblFileTypes` VALUES (14, 'dft', 'application/octet-stream', 0);
INSERT INTO `tblFileTypes` VALUES (15, 'asm', 'text/plain', 0);
INSERT INTO `tblFileTypes` VALUES (16, 'pptx', 'application/vnd.ms-powerpoint', 0);
INSERT INTO `tblFileTypes` VALUES (17, 'docx', 'application/msword', 0);
INSERT INTO `tblFileTypes` VALUES (18, 'xlsx', 'application/vnd.ms-excel', 0);
INSERT INTO `tblFileTypes` VALUES (19, 'mpp', 'application/msproject', 0);
INSERT INTO `tblFileTypes` VALUES (20, 'pps', 'application/vnd.ms-powerpoint', 0);
INSERT INTO `tblFileTypes` VALUES (21, 'mpg', 'video/mpeg', 0);
INSERT INTO `tblFileTypes` VALUES (22, 'avi', 'video/x-msvideo', 0);
INSERT INTO `tblFileTypes` VALUES (23, 'wav', 'audio/x-wav', 0);
INSERT INTO `tblFileTypes` VALUES (24, 'bmp', 'image/bmp', 1);
INSERT INTO `tblFileTypes` VALUES (25, 'dwg', 'application/acad', 0);
INSERT INTO `tblFileTypes` VALUES (26, 'rtf', 'text/rtf', 0);
INSERT INTO `tblFileTypes` VALUES (27, 'mov', 'video/quicktime', 0);
INSERT INTO `tblFileTypes` VALUES (28, 'par', 'application/octat-stream', 0);
INSERT INTO `tblFileTypes` VALUES (29, 'xmcd', 'application/octet-stream', 0);
INSERT INTO `tblFileTypes` VALUES (30, 'kat', '', 0);

--
-- Table structure for table `tblFiles`
--

DROP TABLE IF EXISTS `tblFiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblFiles` (
  `fileID` int(11) NOT NULL AUTO_INCREMENT,
  `reportFK` int(11) NOT NULL DEFAULT '0',
  `filename` text NOT NULL,
  `fileTypeFK` int(11) NOT NULL DEFAULT '0',
  `date_uploaded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `filesize` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fileID`),
  KEY `reportFK` (`reportFK`),
  KEY `fileTypeFK` (`fileTypeFK`)
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tblHelp`
--

DROP TABLE IF EXISTS `tblHelp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblHelp` (
  `helpID` int(11) NOT NULL AUTO_INCREMENT,
  `helpParentFK` int(11) NOT NULL DEFAULT '0',
  `helpTitle` text NOT NULL,
  `helpTxt` text NOT NULL,
  `helpImgA` text NOT NULL,
  `helpRefLetter` text NOT NULL,
  PRIMARY KEY (`helpID`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblHelp`
--

INSERT INTO `tblHelp` VALUES (1, 0, 'About the OSLogbook', '', '', 'A');
INSERT INTO `tblHelp` VALUES (2, 0, 'Viewing reports', '', '', 'B');
INSERT INTO `tblHelp` VALUES (3, 0, 'Adding/Editing reports', '', '', 'C');
INSERT INTO `tblHelp` VALUES (4, 3, 'What are the ''Editors''?', 'The Editors are the different interfaces available within the OSLogbook, through which the user can enter the details of a report.\r\n\r\nAt present, two different editors are available:\r\n\r\n- ASCII Text - This enables the user to enter a simple text report, without the use of any HTML. Reports entered using this Editor will maintain line feeds and whitespace in the same way as when they are entered.\r\n\r\n- CKEditor - This powerful interface allows users to enter reports which incorporate a range of different HTML features, without the need to know any HTML. The Editor enables tasks such as: copying and pasting; lists; table insertion; link insertion and much, much more. Full details on this open source feature can be found on the CKEditor website - <a href="http://ckeditor.com" target="_blank">ckeditor.com</a>.', '', '');
INSERT INTO `tblHelp` VALUES (5, 3, 'What is the Title?', 'The Title is a brief, overall description of the contents of the report.', '', '');
INSERT INTO `tblHelp` VALUES (6, 3, 'What is the Section?', 'The Section is the general area or theme to which the Report belongs. Each section contains at least one Task.', '', '');
INSERT INTO `tblHelp` VALUES (7, 3, 'What is the Task?', 'Each Report must be associated to a specific Task, which describes the specific field in which the Report belongs. The Task itself is associated to the more general, over-arching Section.', '', '');
INSERT INTO `tblHelp` VALUES (8, 3, 'Who are/is the Author(s)?', 'The Author is the person who submitted the Report.', '', '');
INSERT INTO `tblHelp` VALUES (9, 3, 'What do the different buttons do?', 'Four buttons are available to users when entering or editing a Report:\r\n\r\n- Save To Draft - This saves the Report as it is in the current state, allowing a user to either continue adding/editing or to return to the Report at a later date, via the Drafts section.\r\n\r\n- Upload / Manage Files - Opens the Upload Files area, from where users can attach and remove files associated with a Report.\r\n\r\n- Preview - Shows the Report as it would appear in the list of reports available in the Home section. The only difference being that the Report is shown with a red background.\r\n\r\nPost To Logbook - Clicking on this button makes the Report visible to other uses in the list of Reports on the homepage. At this point it can no longer be edited by any non-administration level user.', '', '');
INSERT INTO tblHelp (helpParentFK, helpTitle, helpTxt, helpImgA, helpRefLetter) VALUES (2, 'RSS Feed', 'An RSS feed for the aLOG is available at <href="rss-feed.php">rss-feed.php</a>.', '', '');


--
-- Table structure for table `tblMailNotification`
--

DROP TABLE IF EXISTS `tblMailNotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblMailNotification` (
  `MNID` int(11) NOT NULL AUTO_INCREMENT,
  `taskFK` int(11) NOT NULL DEFAULT '0',
  `userFK` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MNID`),
  KEY `taskFK` (`taskFK`),
  KEY `userFK` (`userFK`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tblReportTags`
--

DROP TABLE IF EXISTS `tblReportTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblReportTags` (
  `reportTagID` int(11) NOT NULL AUTO_INCREMENT,
  `reportFK` int(11) NOT NULL,
  `tagFK` int(11) NOT NULL,
  PRIMARY KEY (`reportTagID`),
  UNIQUE KEY `reportTagidx` (`reportFK`,`tagFK`),
  KEY `reportFKidx` (`reportFK`),
  KEY `tagFKidx` (`tagFK`)
) ENGINE=MyISAM AUTO_INCREMENT=320 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tblReports`
--

DROP TABLE IF EXISTS `tblReports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblReports` (
  `reportID` int(11) NOT NULL AUTO_INCREMENT,
  `taskFK` int(11) NOT NULL DEFAULT '0',
  `reportTitle` text NOT NULL,
  `reportText` text NOT NULL,
  `authorFK` int(11) NOT NULL DEFAULT '0',
  `authorNames` text NOT NULL,
  `parentFK` int(11) NOT NULL DEFAULT '0',
  `editorFK` int(11) NOT NULL DEFAULT '1',
  `postConfirmed` int(11) NOT NULL DEFAULT '0',
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastCommentDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`reportID`),
  KEY `taskFK` (`taskFK`),
  KEY `authorFK` (`authorFK`),
  KEY `parentFK` (`parentFK`)
) ENGINE=MyISAM AUTO_INCREMENT=11176 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

-- 
-- Dumping data for table `tblReports`
-- 

INSERT INTO `tblReports` VALUES (1, 1, 'The OSLogbook', ' Welcome to the OSLogbook application.', 1, 'admin', 0, 1, 1, '2010-05-21 15:18:48');

--
-- Table structure for table `tblSchemaVersion`
--

DROP TABLE IF EXISTS `tblSchemaVersion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblSchemaVersion` (
  `version` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblSchemaVersion`
--

INSERT INTO `tblSchemaVersion` VALUES (4);

--
-- Table structure for table `tblSections`
--

DROP TABLE IF EXISTS `tblSections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblSections` (
  `sectionID` int(11) NOT NULL AUTO_INCREMENT,
  `sectionName` text NOT NULL,
  `sectionColour` varchar(6) NOT NULL DEFAULT '',
  PRIMARY KEY (`sectionID`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

-- 
-- Dumping data for table `tblSections`
-- 

INSERT INTO `tblSections` VALUES (1, 'Logbook Admin', '99FF99');

--
-- Table structure for table `tblTags`
--

DROP TABLE IF EXISTS `tblTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTags` (
  `tagID` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tagID`),
  UNIQUE KEY `tag` (`tag`),
  UNIQUE KEY `tagUKidx` (`tag`)
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTags`
--

INSERT INTO `tblTags` (tag, type) VALUES ('sys:Robot', 1);
INSERT INTO `tblTags` (tag, type) VALUES ('sys:HasAttachments', 1);
INSERT INTO `tblTags` (tag, type) VALUES ('sys:HasImages', 1);

--
-- Table structure for table `tblTasks`
--

DROP TABLE IF EXISTS `tblTasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTasks` (
  `taskID` int(11) NOT NULL AUTO_INCREMENT,
  `sectionFK` int(11) NOT NULL DEFAULT '0',
  `taskName` text NOT NULL,
  PRIMARY KEY (`taskID`),
  KEY `sectionFK` (`sectionFK`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

-- 
-- Dumping data for table `tblTasks`
-- 

INSERT INTO `tblTasks` VALUES (1, 1, 'General');

--
-- Table structure for table `tblUserGroups`
--

DROP TABLE IF EXISTS `tblUserGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblUserGroups` (
  `userGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `userGroup` text NOT NULL,
  PRIMARY KEY (`userGroupID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

-- 
-- Dumping data for table `tblUserGroups`
-- 

INSERT INTO `tblUserGroups` VALUES (2, 'Standard user');
INSERT INTO `tblUserGroups` VALUES (1, 'Administrator');
INSERT INTO `tblUserGroups` VALUES (5, 'Forbidden');

--
-- Table structure for table `tblUsers`
--

DROP TABLE IF EXISTS `tblUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblUsers` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `forename` text NOT NULL,
  `surname` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `lastLogin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userGroupFK` int(11) NOT NULL DEFAULT '0',
  `defaultEditor` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userID`)
) ENGINE=MyISAM AUTO_INCREMENT=3046 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

-- 
-- Dumping data for table `tblUsers`
-- 

INSERT INTO `tblUsers` VALUES (1, '', '', 'admin', 'osl_admin!260508', '', '2010-05-21 15:18:03', 1, 1);

--
-- Table structure for table `tblValueGroups`
--

DROP TABLE IF EXISTS `tblValueGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblValueGroups` (
  `valueGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `valueGroup` text NOT NULL,
  PRIMARY KEY (`valueGroupID`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblValues`
--

INSERT INTO `tblValues` VALUES (1, 1, 'ASCII Text', 'pre');
INSERT INTO `tblValues` VALUES (2, 1, 'CKEditor (HTML)', '');

--
-- Table structure for table `tblValues`
--

DROP TABLE IF EXISTS `tblValues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblValues` (
  `valueID` int(11) NOT NULL AUTO_INCREMENT,
  `valueGroupFK` int(11) NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  `tagCall` text NOT NULL,
  PRIMARY KEY (`valueID`),
  KEY `valueGroupFK` (`valueGroupFK`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-17 11:48:09
